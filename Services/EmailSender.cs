﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace WebApplication.Services
{
    public class EmailSender : IEmailSender
    {
        public IConfiguration Configuration { get; }
        public EmailSender(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            SmtpClient client = new SmtpClient
            {
                Port = Convert.ToInt32(Configuration.GetValue<string>("SendEmailConfig:Port")),
                Host = Configuration.GetValue<string>("SendEmailConfig:Host"), //or another email sender provider
                EnableSsl = Convert.ToBoolean(Configuration.GetValue<string>("SendEmailConfig:EnableSSL")),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = Convert.ToBoolean(Configuration.GetValue<string>("SendEmailConfig:DefaultCredientials")),
                Credentials = new NetworkCredential(Configuration.GetValue<string>("SendEmailConfig:Email"), Configuration.GetValue<string>("SendEmailConfig:Password"))
            };

            return client.SendMailAsync(Configuration.GetValue<string>("SendEmailConfig:Email"), email, subject, htmlMessage);
        }
    }
}
